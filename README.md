# EFS

# TO INITIALIZE THE DIRECTORY PATH 
terraform init

# TO VIEW THE INTENDED CHANGES IN THE INFRASTRUCTURE
terraform plan -var-file="efs.tfvars" 

# TO APPLY THE INTENDED CHANGES IN THE INFRASTRUCTURE
terraform apply -var-file="efs.tfvars"

# TO DELETE THE APPLIED CHANGES IN THE INFRASTRUCTURE
terraform destroy -var-file="efs.tfvars"

# TO INSTALL terraform
yum install wget -y

wget https://releases.hashicorp.com/terraform/0.14.4/terraform_0.14.4_linux_amd64.zip

unzip terraform_0.14.4_linux_amd64.zip

mv terraform /usr/local/bin

export PATH=$PATH:/terraform-path/
